## Acerca del proyecto

Tecnologias usadas:

- [Laravel](https://laravel.com).
- [Composer](https://getcomposer.org).
- PHP
- [XAMPP](https://www.apachefriends.org/es/index.html).

Laravel es accesible, potente y proporciona las herramientas necesarias para aplicaciones grandes y robustas.

## Comandos de instalación

composer install 

npm install 


## Documentación de Uso

Documentación con el paso a paso para el uso y demostración de funcionalidades.
- [Documento](https://docs.google.com/document/d/1fVPyz9SFq2MXACgkR0OpMUP7CZ8ptihXxXaQKWwYbZA/edit?usp=sharing).


## Uso básico

Crud basico con login.

Añade, edita, muestra y elimina registros.

Tiene la función de enviar correos con un destinatario, asunto, mensaje y documentos pdf.

